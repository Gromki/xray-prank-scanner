﻿using UnityEngine;
using System.Collections;

public class PopupManager : MonoBehaviour
{

    [SerializeField]
    GameObject btnPause;                // ссылка на паузу, чтобы скрыть её при экране конца уровня

    [SerializeField]
    GameObject backDark;                // тёмный задник

    [SerializeField]
    GameObject pnlPause;                // панель паузы

    [SerializeField]
    GameObject pnlComplete;             // панель конца уровня

    [SerializeField]
    GameObject pnlStars;                // панель со звёздами
    [SerializeField]
    GameObject[] imgStar;               // массив звёзд для вывода на экран

    [SerializeField]
    GameObject goWin;                   // объект заголовка с информацией о победе
    [SerializeField]
    GameObject goFail;                  // объект заголовка с информацией о проигрыше

    [SerializeField]
    GameObject pnlScores;               // панель с очками
    [SerializeField]
    UnityEngine.UI.Text txtHighScore;   // максимальное значение очков (рекорд)
    [SerializeField]
    UnityEngine.UI.Text txtCurScore;    // текущее значение очков

    public void SetPause(bool value)
    {
        backDark.SetActive(value);
        pnlPause.SetActive(value);
    }

    void OnComplete(bool win)
    {
        btnPause.SetActive(false);
        backDark.SetActive(true);
        pnlComplete.SetActive(true);
        (win ? goWin : goFail).SetActive(true);

        if (GameParameters.useScores)
        {
            pnlScores.SetActive(true);
            txtCurScore.text = GameParameters.scoreCurrent.ToString();
            txtHighScore.text = GameParameters.scoreHigh.ToString();
        }
    }

    /// <summary>Вывести сообщение о том, что уровень завершён</summary>
    /// <param name="win">выиграл ли</param>
    public void LevelComplete(bool win)
    {
        OnComplete(win);
    }

    /// <summary>Вывести сообщение о том, что уровень завершён</summary>
    /// <param name="win">выиграл ли</param>
    /// <param name="stars">сколько звёзд получено (от 0 до 3)</param>
    public void LevelComplete(bool win, int stars)
    {
        OnComplete(win);
        pnlStars.SetActive(true);
        for (int i = 0; i < stars; i++)
            imgStar[i].SetActive(true);
    }
}
