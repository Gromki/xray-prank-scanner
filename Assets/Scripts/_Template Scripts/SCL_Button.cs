﻿using UnityEngine;
using System.Collections;

public class SCL_Button : MonoBehaviour {

    public int levelAttached { get { return myIndex; } }
    int myIndex;                            // индекс кнопки, от 1 и вверх

    UnityEngine.UI.Button btn;              // ссылка на кнопку себя

    [SerializeField]
    UnityEngine.UI.Text txtNumber;          // ссылка на текст, отображающий номер уровня

    [SerializeField]
    UnityEngine.UI.Image[] star;            // картинка звезды

    // ========================================================================================\
    //         звёзды

    [SerializeField]
    GameObject pnlStars;

    void Awake()
    {
        if (btn == null) btn = GetComponent<UnityEngine.UI.Button>();
    }


	// сказать кнопке её индекс
	public void SetIndex (int index) 
    {
        if (btn == null) btn = GetComponent<UnityEngine.UI.Button>();

        myIndex = index;
        txtNumber.text = myIndex.ToString();

        bool unlocked = PlayerPrefs.GetInt("level_" + myIndex + "_unlocked", 0) == 1;
        btn.interactable = unlocked || myIndex == 1;
	}


    // включить звёзды
    public void TurnOnStars()
    {
        int starCount = PlayerPrefs.GetInt("level_" + myIndex + "_stars", 0);

        bool show = btn.interactable && starCount > 0;
        if (show)
        {
            pnlStars.SetActive(true);
            foreach (var s in star)
                s.sprite = Resources.Load<Sprite>("_Template/star_off");

            for (int i = 0; i < starCount; i++)
                star[i].sprite = Resources.Load<Sprite>("_Template/star_on");
        }
    }
}
