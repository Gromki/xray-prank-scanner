﻿using UnityEngine;
using System.Collections;

public class SC_Levels : MScene
{
    [SerializeField]
    bool isStarsAvaliable;      // нужны ли звёзды

    // ========================================================================================\
    //          Внутренние параметры

    [SerializeField]
    Transform goLevels;

    protected override void Awake()
    {
        base.Awake();
        GameParameters.useStarsSystem = isStarsAvaliable;
        SCL_Button btn;
        for (int i = 0; i < goLevels.childCount; i++)
        {
            btn = goLevels.GetChild(i).GetComponent<SCL_Button>();
            btn.SetIndex(i + 1);
            if (isStarsAvaliable) btn.TurnOnStars();
        }
    }


    public void BtnLevel_Click(SCL_Button btn)
    {
        int level = btn.levelAttached;
        print(level);
        //PlayerPrefs.SetInt("currentLevel", level);
        //PlayerPrefs.Save();
        GameParameters.currentLevel = level;
        //Application.LoadLevel("Loading");
        SC_Loading.StartLoadingScene("Game");
    }

    public override void OnBack_Click()
    {
        Application.LoadLevel("Start");
    }
}
