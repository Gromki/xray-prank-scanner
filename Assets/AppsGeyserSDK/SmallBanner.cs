﻿using UnityEngine;
using System.Collections;

public class SmallBanner : FullScreenBanner {
	
	public Texture texture;
	public Texture xButtonTexture;
	public Texture backGroundImage;
	public static SmallBanner Instance;
	public static bool AdMobFlag=false;

	public override void admob(string id,string appsgeyser_click,string appsgeyser_imp)
	{
		AdMobFlag = true;

		OSHookBridge.showAdMobAdSmall (id, appsgeyser_click, appsgeyser_imp);
	}
	// Use this for initialization
	public override void  Start () {
		if (AdMobFlag)
			Destroy (gameObject);
		if (Instance == null)
			Instance = this;
		else
			Destroy (this.gameObject);

		base.Start ();
		//base.action = action;
		double height = Camera.main.orthographicSize * 2.0;
		double width = height * Screen.width / Screen.height;
		transform.localScale = new Vector3((float)width, (float)height, 0.1f);
		DontDestroyOnLoad (gameObject);
	}
	
	IEnumerator Renew(float waitTime) {
		Debug.Log("renew");
		yield return new WaitForSeconds(waitTime);
		GameObject.Instantiate(Resources.Load("SmallBannerObject", typeof(GameObject)));
		Destroy (gameObject);
	}
	
	
	public override  IEnumerator  ReciveImage(string img)
	{
		#if UNITY_EDITOR	
		Hashtable headers = new Hashtable();
		headers.Add("User-Agent","Mozilla/5.0 (iPad; CPU OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5376e Safari/8536.25");
		//"User-Agent", "Mozilla/5.0 (Linux; Android 4.4.2; Nexus 4 Build/KOT49H)");
		#endif
		
		//	Debug.Log ("recive image");
		if (img == null)
			Debug.Log ("url is null");
		
		WWW www = new WWW(img);
		www.threadPriority = ThreadPriority.High;
		yield return www;
		
		texture = www.texture;
		loaded = true;
		if (getString ("refresh") != null) {
			float timeout = float.Parse (getString ("refresh"));
			Debug.Log ("timeout:" + timeout.ToString ());
			StartCoroutine (Renew (timeout));
		}
		
		
	}
	Rect xButtonRect = new Rect (300, 30, 20, 20);
	Rect bannerRect = new Rect (0, 0, 320, 50);
	
	public override void Update()
	{
		base.Update ();
		
		if (loaded)
			base.Impression ();
		Vector3  inputGuiPosition = Input.mousePosition;
		inputGuiPosition.y = Screen.height - inputGuiPosition.y;  
		if(FullScreenBannerSession.Instance==null)
		if (loaded)
		if (Input.GetMouseButtonUp (0)) {
			if (xButtonRect.Contains (inputGuiPosition)) {
				Debug.Log ("close banner");
				Close ();
			}
			
				if (bannerRect.Contains (inputGuiPosition) && !xButtonRect.Contains (inputGuiPosition)) {
				Debug.Log ("onClick fs banner");
				
				if (click != "")
					Application.OpenURL (click);
				Debug.Log (" after onClick fs banner");
				if (clickAppsgeyser != "")
					StartCoroutine(getRequst(clickAppsgeyser));
			}
		}
		

	}
	void OnGUI()
	{

		if(FullScreenBannerSession.Instance==null)
		if(loaded)
				GUI.ModalWindow (1, new Rect (0, 0, 320, 50), fullScreenWindow,  "");
	
		
		
		//GUI.ModalWindow(100,new Rect (0, 0, Screen.width, Screen.height),fullScreenWindow,"test window");
		
	}
	void Close()
	{
		if(action=="onclose")
			GlobalObject.Instance.QuitApplication();	
		Destroy (gameObject);
	}
	
	void fullScreenWindow(int windowId)
	{

		//Rect bannerRect = new Rect (Screen.width / 10, Screen.height / 10, Screen.width - Screen.width / 5, Screen.height - Screen.height / 5);
		if(loaded)
		{
			//GUI.DrawTexture(new Rect(0,0,Screen.width,Screen.height),backGroundImage);
			GUI.DrawTexture(bannerRect,texture);
			GUI.DrawTexture(xButtonRect,xButtonTexture);
		
		}
	}
}
