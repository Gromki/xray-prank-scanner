﻿using UnityEngine;
using System.Collections;

public class GameController : SC_Game
{
    public Transform scan;

    int target = 0;
    void Awake ()
    {
        #region necessarily
        base.Awake();       // оставить, чтобы работали штуки шаблона
        #endregion
    }

    // код игровой логики писать тут, метод работает пока не пауза
    protected override void GameUpdate()
    {
        if (scan.position.y >= 48f)
        {
            target = 1;
        }

        if (scan.position.y <= -40)
        {
            target = 0;
        }

        switch (target)
        {
            case 0:
                scan.Translate(Vector2.up * Time.deltaTime * 30);
                break;
            case 1:
                scan.Translate(-Vector2.up * Time.deltaTime * 30);
                break;

        }
    }

    // код игровой логики С ФИЗИКОЙ писать тут, метод работает пока не пауза
    protected override void GameFixedUpdate()
    {

    }
}
