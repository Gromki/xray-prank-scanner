﻿using UnityEngine;
using System.Collections;

public class GlobalObject : MonoBehaviour {
	
	public static GlobalObject Instance { get; private set; }
	public bool messageSend = false;

	void Update()
	{

	}
	void Awake() 
	{
		if (Instance)
		{
			DestroyImmediate(gameObject);
		}
		else
		{
			DontDestroyOnLoad(gameObject);
			Instance = this;
		}
	}

	void Start()
	{
		GlobalMessage.Instance.CancelNotifications ();
	}

	void OnApplicationPause(bool pauseStatus) 
	{
		if(pauseStatus == true)
		{
		//	Saver ();
			Messager ();
		}
		else
		{
			GlobalMessage.Instance.CancelNotifications ();
		}
	}
	
	public void QuitApplication()
	{
	//	Saver ();
		Messager ();
		Application.Quit ();
	}

	void OnApplicationQuit() 
	{
	//	Saver ();
		Messager ();
	}

	 void Messager()
	{
		if(messageSend == true)
			return;

		messageSend = true;

		GlobalMessage.Instance.CancelNotifications ();

	//	GlobalMessage.Instance.PushAfter (5, GlobalWord.Instance.GetNexTitle(), GlobalWord.Instance.GetNexWord());
	

		int hours24 = 86400;
		GlobalMessage.Instance.PushAfter (hours24, GlobalWord.Instance.GetNexTitle(), GlobalWord.Instance.GetNexWord());
		//GlobalMessage.Instance.PushAfter (20, "Тест 24 часов", hours24+" секунд");


		int hours48 = 172800;
		GlobalMessage.Instance.PushAfter (hours48, GlobalWord.Instance.GetNexTitle(), GlobalWord.Instance.GetNexWord());
		//GlobalMessage.Instance.PushAfter (20, "Тест 48 часов", hours48+" секунд");

		int hours120 = 432000;
		GlobalMessage.Instance.PushAfter (hours120, GlobalWord.Instance.GetNexTitle(), GlobalWord.Instance.GetNexWord());
		//GlobalMessage.Instance.PushAfter (20, "Тест 120 часов", hours120+" секунд");

		/**
		int fullRestore = (GlobalEnergy.Instance.MaxEnergy - GlobalEnergy.Instance.Energy)*GlobalEnergy.Instance.energyCooldown;
		if(fullRestore > 60)
		{
			GlobalMessage.Instance.PushAfter (fullRestore, "Gas restored!", GlobalWord.Instance.GetNexWord());
			//GlobalMessage.Instance.PushAfter (25, "Тест востановления", fullRestore+" секунд");
		}

		int waitSeconds = 3600;
		if(waitSeconds < fullRestore+60)
		{
			GlobalMessage.Instance.PushAfter (waitSeconds, "Do not forget to check out!", "After "+SecondsToTime(fullRestore-waitSeconds)+" gasoline is fully restored!");
		}

**/
		//GlobalMessage.Instance.PushAfter (15, "This is test message", "This is tes message");
		//GlobalMessage.Instance.PushAfter (25, "This is test message", "debug");
	}

	public void Saver()
	{
		PlayerPrefs.SetString ("QuitTime",GlobalTime.Instance.getTimeString());
		PlayerPrefs.SetInt ("Energy",GlobalEnergy.Instance.Energy);
		PlayerPrefs.Save();
	}
	
	string SecondsToTime(int sec)
	{
		var ts = System.TimeSpan.FromSeconds(sec);
		string time = "00:00:00";
		//Debug.Log (ts);
		time = ""+ts;
		return time;
	}

}
