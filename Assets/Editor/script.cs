using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System;

public class script : MonoBehaviour {
	static string GetProjectName(){
		string[] s = Application.dataPath.Split('/');
		return s[s.Length - 2];
	}
	
	static string[] GetScenePaths() {
        int enabledScenesCount = 0;
        List<string> scenes = new List<string>();

        for (int i = 0; i < EditorBuildSettings.scenes.Length; i++) {
            if (EditorBuildSettings.scenes[i].enabled) {
                enabledScenesCount++;
                scenes.Add(EditorBuildSettings.scenes[i].path);
            }
        }
		
		//for(int i = 0; i < scenes.Length; i++){
		//	scenes[i] = EditorBuildSettings.scenes[i].path;
		//}
		return scenes.ToArray();
	}
	
	[MenuItem("File/AutoBuilder/iOS")]
	public static void PerformiOSBuild (){
		EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTarget.iOS);
		BuildPipeline.BuildPlayer(GetScenePaths(), "Builds/iOS",BuildTarget.iOS,BuildOptions.None);
	}
	
	public static void BuildStuff(){
		StreamReader reader = new StreamReader(Application.dataPath + "/AppsGeyserSDK/Resources/apikey", Encoding.Default);
		string apikey = reader.ReadLine();
		string companyName = reader.ReadLine();
		string name = reader.ReadLine();
		string pacname = reader.ReadLine();
		string outPath = reader.ReadLine();
		string keyName = reader.ReadLine();
		string keyPass = reader.ReadLine();
		string aliName = reader.ReadLine();
		string aliPass = reader.ReadLine();
		string banndleVer = reader.ReadLine();
		string bundleVerCode = reader.ReadLine();
		reader.Close();
		PerformAndroidOSBuild(companyName, name, pacname, outPath,apikey, keyName,keyPass,aliName, aliPass, banndleVer, bundleVerCode);
	}
	
	[MenuItem("File/AutoBuilder/Android")]
	static void PerformAndroidOSBuild (string companyName, string name,string pacname,string outPath,string apikey, string keyName,string keyPass,string aliName, string aliPass, string banndleVer, string bundleVerCode){
		EditorApplication.OpenScene ( EditorBuildSettings.scenes[0].path);
		GameObject sdk_o=GameObject.Find("_AppsgeyserSDK");
		if (sdk_o == null) {
			Debug.Log ("__AppsgeyserSDK");
			sdk_o = GameObject.Find ("__AppsgeyserSDK");
		}
		
		if(sdk_o!=null)
			sdk_o.SetActive (false);
		sdk_o = null;
		if(sdk_o == null){
			Debug.Log ("if(sdk_o == null)");
			UnityEngine.Object sdk = AssetDatabase.LoadAssetAtPath("Assets/AppsGeyserSDK/_AppsgeyserSDK.prefab", typeof(GameObject));
			sdk_o = Instantiate(sdk, Vector3.zero, Quaternion.identity) as GameObject;
			
			sdk_o.name = "_AppsgeyserSDK";
		}
		
		PluginController pc = sdk_o.GetComponent ("PluginController")as PluginController;
		Debug.Log ("PluginController pc = sdk_o.GetComponent");
		pc.apiKey=apikey;Debug.Log ("pc.apiKey=apikey");
		Debug.Log ("pc.apiKey="+pc.apiKey);
		Debug.Log ("apiKey="+apikey);
		//	EditorApplication.SaveScene();
		EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTarget.Android);
		EditorUserBuildSettings.SetBuildLocation(BuildTarget.Android,"D:/");
		
		if(keyName != "") 
			PlayerSettings.Android.keystoreName = keyName;
		if(keyPass != "")
			PlayerSettings.Android.keystorePass = keyPass;
		if(aliName != "")
			PlayerSettings.Android.keyaliasName = aliName;
		if(aliPass != "")
			PlayerSettings.Android.keyaliasPass = aliPass;
		//		if(true /*aliPass != ""*/)
		//			PlayerSettings.Android.bundleVersionCode = 1; //TODO getting bundleVersionCode from autobuilder
		if(companyName != "")
			PlayerSettings.companyName = companyName;
		if(name != "")
			PlayerSettings.productName = name;
		if(pacname != "")
			PlayerSettings.bundleIdentifier = pacname;
		//		if(banndleVer != "")
		//			PlayerSettings.bundleVersion = banndleVer;
		
		int unixTimestamp = (int)(DateTime.UtcNow.Subtract(new DateTime(2014, 10, 6))).TotalHours/4 + 1;
		//if(banndleVer != "")To
		PlayerSettings.Android.bundleVersionCode = unixTimestamp;//banndleVer;
		
		
		
		IconLoader();
		PerformAndroidBuild (outPath);
	}
	
	
	static void IconLoader(){
		if(IsThereAllIcons()){
			TextureImporter i36=  TextureImporter.GetAtPath("Assets/AppsGeyserSDK/Resources/icon36.png") as TextureImporter;
			TextureImporter i48=  TextureImporter.GetAtPath("Assets/AppsGeyserSDK/Resources/icon48.png") as TextureImporter;
			TextureImporter i72=  TextureImporter.GetAtPath("Assets/AppsGeyserSDK/Resources/icon72.png") as TextureImporter;
			TextureImporter i96=  TextureImporter.GetAtPath("Assets/AppsGeyserSDK/Resources/icon96.png") as TextureImporter;
			TextureImporter i144=  TextureImporter.GetAtPath("Assets/AppsGeyserSDK/Resources/icon144.png") as TextureImporter;
			TextureImporterSettings settings   = new TextureImporterSettings();
			i36.ReadTextureSettings(settings);
			i48.ReadTextureSettings(settings);
			i72.ReadTextureSettings(settings);
			i96.ReadTextureSettings(settings);
			i144.ReadTextureSettings(settings);
			
			//settings.ApplyTextureType(TextureImporterType.GUI, false);
			///	i.SetTextureSettings(settings);
			i36.SetPlatformTextureSettings("Android", 1024, TextureImporterFormat.AutomaticTruecolor, 0);
			i48.SetPlatformTextureSettings("Android", 1024, TextureImporterFormat.AutomaticTruecolor, 0);
			i72.SetPlatformTextureSettings("Android", 1024, TextureImporterFormat.AutomaticTruecolor, 0);
			i96.SetPlatformTextureSettings("Android", 1024, TextureImporterFormat.AutomaticTruecolor, 0);
			i144.SetPlatformTextureSettings("Android", 1024, TextureImporterFormat.AutomaticTruecolor, 0);
			
			i36.SetTextureSettings(settings);
			i48.SetTextureSettings(settings);
			i72.SetTextureSettings(settings);
			i96.SetTextureSettings(settings);
			i144.SetTextureSettings(settings);
			
			AssetDatabase.ImportAsset("Assets/AppsGeyserSDK/Resources/icon36.png", ImportAssetOptions.ForceSynchronousImport);
			AssetDatabase.ImportAsset("Assets/AppsGeyserSDK/Resources/icon48.png", ImportAssetOptions.ForceSynchronousImport);
			AssetDatabase.ImportAsset("Assets/AppsGeyserSDK/Resources/icon72.png", ImportAssetOptions.ForceSynchronousImport);
			AssetDatabase.ImportAsset("Assets/AppsGeyserSDK/Resources/icon96.png", ImportAssetOptions.ForceSynchronousImport);
			AssetDatabase.ImportAsset("Assets/AppsGeyserSDK/Resources/icon144.png", ImportAssetOptions.ForceSynchronousImport);
			//	TextureImporterSettings set = new TextureImporterSettings();
			//	ti.SetTextureSettings(set);
			
			
			
			
			
			
			
			Texture2D[] iconsAndroid = new Texture2D[5];
			TextureImporter iconImporter = new TextureImporter();
			
			//iconImporter
			
			
			iconsAndroid[0] = (Texture2D)AssetDatabase.LoadAssetAtPath("Assets/AppsGeyserSDK/Resources/icon144.png", typeof(Texture2D));
			iconsAndroid[1] = (Texture2D)AssetDatabase.LoadAssetAtPath("Assets/AppsGeyserSDK/Resources/icon96.png", typeof(Texture2D));
			iconsAndroid[2] = (Texture2D)AssetDatabase.LoadAssetAtPath("Assets/AppsGeyserSDK/Resources/icon72.png", typeof(Texture2D));
			iconsAndroid[3] = (Texture2D)AssetDatabase.LoadAssetAtPath("Assets/AppsGeyserSDK/Resources/icon48.png", typeof(Texture2D));
			iconsAndroid[4] = (Texture2D)AssetDatabase.LoadAssetAtPath("Assets/AppsGeyserSDK/Resources/icon36.png", typeof(Texture2D));
			
			PlayerSettings.SetIconsForTargetGroup(BuildTargetGroup.Android, iconsAndroid);
		}
	}
	
	static bool IsThereAllIcons(){
		if(AssetDatabase.LoadAssetAtPath("Assets/AppsGeyserSDK/Resources/icon144.png", typeof(Texture2D)) == null)
			return false;
		if(AssetDatabase.LoadAssetAtPath("Assets/AppsGeyserSDK/Resources/icon96.png", typeof(Texture2D)) == null)
			return false;
		if(AssetDatabase.LoadAssetAtPath("Assets/AppsGeyserSDK/Resources/icon72.png", typeof(Texture2D)) == null)
			return false;
		if(AssetDatabase.LoadAssetAtPath("Assets/AppsGeyserSDK/Resources/icon48.png", typeof(Texture2D)) == null)
			return false;
		if(AssetDatabase.LoadAssetAtPath("Assets/AppsGeyserSDK/Resources/icon36.png", typeof(Texture2D)) == null)
			return false;
		return true;
	}
	
	static void GetIconFromAssets(){
		
	}
	
	public static string RemoveSpecialCharacters(string str) {
		StringBuilder sb = new StringBuilder();
		foreach (char c in str) {
			if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || c == '.' || c == '_') {
				sb.Append(c);
			}
		}
		return sb.ToString();
	}
	
	[MenuItem("File/AutoBuilder/Android")]
	static void PerformAndroidBuild (string outPath){
		EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTarget.Android);
		BuildPipeline.BuildPlayer(GetScenePaths(),outPath,BuildTarget.Android,BuildOptions.None);
	}
}