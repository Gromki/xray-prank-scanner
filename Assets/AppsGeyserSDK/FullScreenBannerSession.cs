﻿using UnityEngine;
using System.Collections;

public class FullScreenBannerSession : FullScreenBanner {
	
	public Texture texture;
	public Texture xButtonTexture;
	public Texture backGroundImage;
	public static FullScreenBannerSession Instance;
	// Use this for initialization
	public override void Start () {
		OSHookBridge.ShowAdMobSmall (false);
		Instance = this;
		base.Start ();
		//base.action = action;
		double height = Camera.main.orthographicSize * 2.0;
		double width = height * Screen.width / Screen.height;
		transform.localScale = new Vector3((float)width, (float)height, 0.1f);
		DontDestroyOnLoad (gameObject);
	}
	
	
	
	
	public override  IEnumerator  ReciveImage(string img)
	{
		#if UNITY_EDITOR	
		Hashtable headers = new Hashtable();
		headers.Add("User-Agent","Mozilla/5.0 (iPad; CPU OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5376e Safari/8536.25");
		//"User-Agent", "Mozilla/5.0 (Linux; Android 4.4.2; Nexus 4 Build/KOT49H)");
		#endif
		
		//	Debug.Log ("recive image");
		if (img == null)
			Debug.Log ("url is null");
		
		WWW www = new WWW(img);
		www.threadPriority = ThreadPriority.High;
		yield return www;
		
		texture = www.texture;
		loaded = true;
		
	}
	Rect xButtonRect = new Rect (0, 0, Screen.width / 10, Screen.height / 10);
	Rect bannerRect = new Rect (Screen.width / 10, Screen.height / 10, Screen.width - Screen.width / 5, Screen.height - Screen.height / 5);
	
	public override void Update()
	{
		if (IsLouded ())
			OSHookBridge.ShowAdMobSmall (false);
		base.Update ();
		
		if (loaded)
			base.Impression ();
		Vector3  inputGuiPosition = Input.mousePosition;
		inputGuiPosition.y = Screen.height - inputGuiPosition.y;  
		if (loaded)
		if (Input.GetMouseButtonUp (0)) {
			if (xButtonRect.Contains (inputGuiPosition)) {
				Debug.Log ("close banner");
				Close ();
			}
			
			if (bannerRect.Contains (inputGuiPosition)) {
				Debug.Log ("onClick fs banner");
				
				if (click != "")
					Application.OpenURL (click);
				Debug.Log (" after onClick fs banner");
				if (clickAppsgeyser != "")
					StartCoroutine(getRequst(clickAppsgeyser));
				Close();
			}
		}
		
		if(Input.GetKeyDown(KeyCode.Escape))
			Close();
	}
	void OnGUI()
	{
		if(loaded)
			GUI.ModalWindow (1, new Rect (0, 0, Screen.width, Screen.height), fullScreenWindow,  "");
		/**
			if(loaded && false)
		{
			GUI.DrawTexture(new Rect(0,0,Screen.width,Screen.height),backGroundImage);
			GUI.DrawTexture(xButtonRect,xButtonTexture);
			GUI.DrawTexture(bannerRect,texture);
		}
	
	**/
		
		
		//GUI.ModalWindow(100,new Rect (0, 0, Screen.width, Screen.height),fullScreenWindow,"test window");
		
	}
	void Close()
	{
		OSHookBridge.ShowAdMobSmall (true);
		if(action=="onclose")
			GlobalObject.Instance.QuitApplication();	
		Destroy (gameObject);
	}
	
	void fullScreenWindow(int windowId)
	{
		Rect xButtonRect = new Rect (0, 0, Screen.width / 10, Screen.height / 10);
		Rect bannerRect = new Rect (Screen.width / 10, Screen.height / 10, Screen.width - Screen.width / 5, Screen.height - Screen.height / 5);
		if(loaded)
		{
			GUI.DrawTexture(new Rect(0,0,Screen.width,Screen.height),backGroundImage);
			GUI.DrawTexture(xButtonRect,xButtonTexture);
			GUI.DrawTexture(bannerRect,texture);
		}
	}
}
