﻿using UnityEngine;
using System.Collections;

public class GlobalWord : MonoBehaviour {
	public static GlobalWord Instance { get; private set; }
	public string[] words;
	//public string[] titles;
	private  int index=-1;
	private string productName;
	private string placeHolder="_producName_";
	// Use this for initialization
	void Awake() 
	{

		index=PlayerPrefs.GetInt("Push index");
		Instance = this;
		productName = OSHookBridge.GetProductName ();
	}


	public  string GetNexTitle()
	{
	

		//string title = titles [index];
		//title = title.Replace (placeHolder, productName);
		return productName;
	}
	public  string GetNexWord()
	{
		index++;
		if (index > (words.Length - 1))
			index = 0;
		PlayerPrefs.SetInt ("Push index", index);
		string word = words [index];

		word = word.Replace (placeHolder, productName);
		return word;
	}
	public string getRandomWord()
	{
		int i = (int)(Random.Range(0, words.Length - 1));
		             
		return words[i];
	}


}
