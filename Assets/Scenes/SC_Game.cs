﻿using UnityEngine;
using System.Collections;

public class SC_Game : MScene
{

    [SerializeField]
    PopupManager popupManager;              // ссылка на менеджер всплывающих окон

    public int currentLevel { get { return GameParameters.currentLevel; } }
    public int score { get { return GameParameters.scoreCurrent; } set { GameParameters.scoreCurrent = value; } }

    #region Parameter Keeper
    float oldTimeScale;
    #endregion

    /// <summary>На паузе игра или нет</summary>
    protected bool isPause;


    protected override void Awake()
    {
        base.Awake();
        score = 0;
        Time.timeScale = 1f;
        if (currentLevel > 1) OSHookBridge.TryToShowBanner();
    }


    public override void OnBack_Click()
    {
        Application.LoadLevel(GameParameters.useLevels ? "Levels" : "Start");
    }

    void Update()
    {
        base.Update();
        if (!isPause) GameUpdate();
    }

    /// <summary>Основной метод для игры</summary>
    protected virtual void GameUpdate()
    { }


    void FixedUpdate()
    {
        base.FixedUpdate();
        if (!isPause) GameFixedUpdate();
    }

    /// <summary>Основной метод для игры</summary>
    protected virtual void GameFixedUpdate()
    { }

    #region complete methods
    // набор обязательных действий при завершении уровня
    void OnCompleteActions(bool win)
    {
        isPause = true;
        oldTimeScale = Time.timeScale;
        Time.timeScale = 0f;

        // разблокировать следующий уровень
        if (win) PlayerPrefs.SetInt("level_" + (currentLevel + 1) + "_unlocked", 1);

        if (GameParameters.useScores)
        {
            GameParameters.scoreHigh = PlayerPrefs.GetInt("HighScore", 0);
            if (score > GameParameters.scoreHigh)
            {
                GameParameters.scoreHigh = score;
                PlayerPrefs.SetInt("HighScore", score);
            }

        }

        PlayerPrefs.Save();
    }


    /// <summary>Вывести сообщение о том, что уровень завершён</summary>
    /// <param name="win">выиграл ли</param>
    public void LevelComplete(bool win)
    {
        if (!isPause)
        {
            OnCompleteActions(win);
            popupManager.LevelComplete(win);
        }
    }

    /// <summary>Вывести сообщение о том, что уровень завершён</summary>
    /// <param name="win">выиграл ли</param>
    /// <param name="starsCount">сколько звёзд получено (от 0 до 3)</param>
    public void LevelComplete(bool win, int starsCount)
    {
        if (!isPause)
        {
            if (!GameParameters.useStarsSystem) Debug.LogWarning("Вы выводите звёзды за прохождение, но не включили их на сцене уровней.");

            // записать полученные звёзды на уровне, если их больше, чем было уже получено
            int curStars = PlayerPrefs.GetInt("level_" + currentLevel + "_stars", 0);
            if (starsCount > curStars) PlayerPrefs.SetInt("level_" + currentLevel + "_stars", starsCount);

            OnCompleteActions(win);

            popupManager.LevelComplete(win, starsCount);
        }
    }
    #endregion


    #region popup buttons
    public void BtnPause_Click()
    {
        if (!isPause)
        {
            isPause = true;
            oldTimeScale = Time.timeScale;
            Time.timeScale = 0f;
            popupManager.SetPause(true);
        }
    }

    public void BtnMenu_Click()
    {
        Time.timeScale = oldTimeScale;
        Application.LoadLevel("Start");
    }

    public void BtnRestart_Click()
    {
        OSHookBridge.TryToShowBanner();

        Time.timeScale = oldTimeScale;
        Application.LoadLevel("Game");
    }

    public void BtnResume_Click()
    {
        popupManager.SetPause(false);
        isPause = false;
        Time.timeScale = oldTimeScale;
    }

    public void BtnNext_Click()
    {
        Time.timeScale = oldTimeScale;
        if (!GameParameters.useLevels) Debug.LogWarning("Вы уверены, что вам нужна эта кнопка?");
        Application.LoadLevel(GameParameters.useLevels ? "Levels" : "Start");
    }
    #endregion
}
