﻿using UnityEngine;
using System.Collections;

public class GlobalEnergy : MonoBehaviour 
{
	public static GlobalEnergy Instance { get; private set; }
	
	public int Energy = 46;
	public int MaxEnergy = 46;
	public int energyCooldown = 420;
	public int restorRate = 1;
	public int oneRestoreTimer;
	float myTimer;
	
	bool needFirstScore = false;
	
	System.DateTime parsedTimeClass;
	System.DateTime quitTimeClass;
	System.DateTime nowTimeClass;
	System.DateTime restoreTimeClass;
	
	public void Awake()
	{
		if (Instance)
		{
			DestroyImmediate(gameObject);
		}
		else
		{
			DontDestroyOnLoad(gameObject);
			Instance = this;
		}
	}
	
	void Start()
	{
		prepareTimeClass ();
		
		string quitTime = PlayerPrefs.GetString ("QuitTime");
		if(quitTime == "" || quitTime==null)
			Energy = MaxEnergy;
		else
			Energy = PlayerPrefs.GetInt ("Energy");
		
		needFirstScore = true;

		
	}
	
	void Update()
	{
		if(myTimer > 0)
		{
			myTimer -= Time.deltaTime;
			oneRestoreTimer = Mathf.RoundToInt(myTimer);
			//Debug.Log(myTimer+" : "+oneRestoreTimer);
		}

		if(needFirstScore && GlobalTime.Instance.timeSeted)
		{
			nowTimeClass = GlobalTime.Instance.getTimeClass();
			needFirstScore = false;
			compareTime();
		}


	}

	void prepareTimeClass()
	{
		string quitTime = PlayerPrefs.GetString ("QuitTime");
		//if game started first time
		if(!ParseTimeString(quitTime))
			quitTimeClass = GlobalTime.Instance.getTimeClassFast();
		else
			quitTimeClass = parsedTimeClass;
		
		
		
		string restoreTime = PlayerPrefs.GetString ("RestoreTime");
		if (!ParseTimeString (restoreTime)) 
		{
			restoreTimeClass = GlobalTime.Instance.getTimeClassFast ();
			PlayerPrefs.SetString ("RestoreTime",GlobalTime.Instance.getTimeString());
		}
		else
		{
			restoreTimeClass = parsedTimeClass;
		}
	}
	
	void compareTime()
	{
		System.TimeSpan timeLeft = nowTimeClass - restoreTimeClass;
		int leftSeconds = (int)timeLeft.TotalSeconds;
		//Debug.Log ("seconds after restore: "+leftSeconds);
		
		int restoreTicks = Mathf.FloorToInt(leftSeconds / energyCooldown);
		PlusManyEnergy (restoreTicks*restorRate);
		
		int offset = leftSeconds - (energyCooldown * restoreTicks);
		
		//хуета какаято
		int subtract = leftSeconds - offset;
		System.DateTime newTime;
		newTime = restoreTimeClass.AddSeconds (subtract);
		PlayerPrefs.SetString ("RestoreTime",timeClassToString(newTime));
		
		//Debug.Log ("free seconds after restore: "+offset);
		
		StartCoroutine(RestoreEnergyRepeater(offset));
	}
	
	public void MinusEnergy()
	{
		if(Energy <= 0)
			return;
		
		Energy = Energy - restorRate;
		PlayerPrefs.SetInt ("Energy",Energy);
	}
	
	public void MinusManyEnergy(int count)
	{
		int tempEnergy = Energy - count;
		if(tempEnergy >= 0)
			Energy = tempEnergy;
		else
			Energy = 0;
		PlayerPrefs.SetInt ("Energy",Energy);
	}
	
	public void PlusEnergy()
	{
		if(Energy >= MaxEnergy)
			return;
		
		Energy = Energy + restorRate;
		PlayerPrefs.SetInt ("Energy",Energy);
	}
	
	public void PlusManyEnergy(int count)
	{
		int tempEnergy = Energy + count;
		if(tempEnergy <= MaxEnergy)
			Energy = tempEnergy;
		else
			Energy = MaxEnergy;

		PlayerPrefs.SetInt ("Energy",Energy);
	}
	
	IEnumerator RestoreEnergyRepeater(int offset)
	{
		while(true)
		{
			myTimer = energyCooldown-offset;
			yield return new WaitForSeconds(energyCooldown-offset);
			Debug.Log("energyRestored");
			PlusEnergy();
			offset = 0;
			PlayerPrefs.SetString ("RestoreTime",GlobalTime.Instance.getTimeString());
			Debug.Log("new RestoreTime: "+PlayerPrefs.GetString ("RestoreTime"));
		}
	}
	bool ParseTimeString(string timeS)
	{
		//year+"-"+month+"-"+day+" "+hour+":"+minute+":"+seconds
		
		int localyear = 0;
		int localmonth = 0;
		int localday = 0;
		
		int localhour = 0;
		int localminute = 0;
		int localseconds = 0;
		
		bool timeStringParsed = true;
		
		string [] split = timeS.Split(':',' ','-');
		if(split.Length > 5)
		{
			if(!int.TryParse(split[0], out localyear))
				timeStringParsed = false;
			if(!int.TryParse(split[1], out localmonth))
				timeStringParsed = false;
			if(!int.TryParse(split[2], out localday))
				timeStringParsed = false;
			
			if(!int.TryParse(split[3], out localhour))
				timeStringParsed = false;
			if(!int.TryParse(split[4], out localminute))
				timeStringParsed = false;
			if(!int.TryParse(split[5], out localseconds))
				timeStringParsed = false;
			
			if(timeStringParsed)
			{
				parsedTimeClass = new System.DateTime(localyear, localmonth, localday, localhour, localminute, localseconds);
			}
			
		}
		else 
		{
			timeStringParsed = false;
		}  
		
		return timeStringParsed;
	}
	
	string timeClassToString(System.DateTime tC)
	{
		string timeString = tC.Year+"-"+tC.Month+"-"+tC.Day+" "+tC.Hour+":"+tC.Minute+":"+tC.Second;
		return timeString;
	}
	
}
