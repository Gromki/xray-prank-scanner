﻿using UnityEngine;
using System.Collections;

public class AppsgeyserCamera : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;
		if (Physics.Raycast (transform.position,transform.forward, out hit, 100)) 
		{
			if(hit.collider.gameObject.tag=="fsBanner")
			{
				GameObject go=hit.collider.gameObject;
				FullScreenBanner fsBannerComponent=(FullScreenBanner)go.GetComponent<FullScreenBanner>() as FullScreenBanner;
				fsBannerComponent.Impression();
			}
		}

	}
}
