﻿using UnityEngine;
using System.Collections;

public class SC_Loading : MScene
{
    static string loadingScene;
    AsyncOperation game;
    [SerializeField]
    Transform loadingBar;
    [SerializeField]
    UnityEngine.UI.Image promoBoard;
    [SerializeField]
    Sprite[] promoScreen;

    void Start()
    {
        game = Application.LoadLevelAsync(loadingScene);
        game.allowSceneActivation = false;
        StartCoroutine(Promo());
    }

    void Update()
    {
        //loadingBar.Rotate(new Vector3(0, 0, -20 * Time.deltaTime));
    }

    public static void StartLoadingScene(string scene)
    {
        loadingScene = scene;
        Application.LoadLevel("Loading");
    }

    // показывает фэейковые скрины при загрузке
    IEnumerator Promo()
    {
        foreach (var s in promoScreen)
        {
            promoBoard.sprite = s;
            yield return new WaitForSeconds(0.5f);
        }
        game.allowSceneActivation = true;
    }
}
