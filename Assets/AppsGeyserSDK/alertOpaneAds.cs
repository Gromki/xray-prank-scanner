﻿using UnityEngine;
using System.Collections;

public class alertOpaneAds : MonoBehaviour {

	// 200x300 px window will apear in the center of the screen.
	private Rect windowRect = new Rect ((Screen.width - 200)/2, (Screen.height - 300)/2, 200, 150);
	// Only show it if needed.
	private bool show = true;
	private string click;
	private string clickAppsgeyser;

	public void setLink (string _click,string _clickAppsgeyser)
	{

		click = _click;
		clickAppsgeyser = _clickAppsgeyser;
	}
	public  virtual IEnumerator getRequst(string url)
	{
		WWW www = new WWW(url);
		www.threadPriority = ThreadPriority.High;
		Debug.Log ("send req:"+url);
		yield return www;
	}
	void OnGUI () 
	{
		if(show)
			windowRect = GUI.Window (0, windowRect, DialogWindow, "Open this ad?");
	}
	
	// This is the actual window.
	void DialogWindow (int windowID)
	{
		float y = 20;
	//	GUI.Label(new Rect(5,y, windowRect.width, 25), "Open this ad?");
		
		if(GUI.Button(new Rect(5,y, windowRect.width - 10, 25), "Open"))
		{
			if(click!="")
				Application.OpenURL(click);
			if(clickAppsgeyser!="")
				getRequst(clickAppsgeyser);
			show = false;
			Destroy(this);
		}
		
		if(GUI.Button(new Rect(5,y*3, windowRect.width - 10, 25), "Cancel"))
		{
			Destroy(this);
			show = false;
		}
	}
	
	// To open the dialogue from outside of the script.
	public void Open()
	{
		show = true;
	}
}	