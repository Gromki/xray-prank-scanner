﻿using UnityEngine;
using System.Collections;
using MiniJSON;
public class FullScreenBanner : MonoBehaviour {
	protected bool loaded=false;
	 protected Texture image;
	public string click;
	protected string img;
	protected	string clickAppsgeyser;
	public	string imp;
	public	string impAppsgeyser;
	protected bool impression=false;
	protected bool load=false;
	protected string reqJson;
	protected string adMob_Click;
	public zone zoneNumber;
	public string action;
	
	
	public  bool IsLouded()
	{
		return loaded;
	}

	public virtual void admob(string id,string appsgeyser_click,string appsgeyser_imp)
	{
		
		if (action=="onclose")
			OSHookBridge.showAdMobFullScreenBannerEscape(id,appsgeyser_click,appsgeyser_imp);
		else
		OSHookBridge.showAdMobFullScreenBanner(id,appsgeyser_click,appsgeyser_imp);
	}

	IEnumerator loadBanner()
	{
		string url;
		#if UNITY_ANDROID	
		url=OSHookBridge.GetUnityUrl()+"&action="+action;	
	//	url = "http://unitu3dads.appsgeyser.com/ios.php?widgetid=test&guid=01a05870-a04a-49fb-a275-f52e0b4917c9&v=0.158&advid=e069a102-d2c8-4c27-b81a-6eafeb64bbf2&limit_ad_tracking_enabled=true&tlat=0.0&tlon=0.0&p=android&action=onstart&format=json";

		#endif
		#if UNITY_EDITOR
		url = "http://unitu3dads.appsgeyser.com/index.php?widgetid=test&guid=01a05870-a04a-49fb-a275-f52e0b4917c9&v=0.158&advid=e069a102-d2c8-4c27-b81a-6eafeb64bbf2&limit_ad_tracking_enabled=true&tlat=0.0&tlon=0.0&p=android&action=small&format=json";
		#endif
		#if UNITY_EDITOR	
		Hashtable headers = new Hashtable();
		headers.Add("User-Agent", "Mozilla/5.0 (Linux; Android 4.4.2; Nexus 4 Build/KOT49H)");
		#endif
		WWW www = new WWW(url);
		www.threadPriority = ThreadPriority.High;
		yield return www;
		reqJson =www.text;
		Debug.Log ("data:"+www.text);
		Debug.Log (reqJson);
		img = getString ("img");
		imp = getString ("imp_pixel");
		impAppsgeyser = getString ("appsgeyser_imp");
		clickAppsgeyser = getString ("appsgeyser_click");
		if (www.text == "no_data")
			Destroy (this);
		if (getString ("adtype") == "admob") 
		{
			string id = getString("ad_unit_id");
			Debug.Log("da_unit_id:"+id);
		if(id!=null)
			if(id!="")
			{

				admob(id,clickAppsgeyser,impAppsgeyser);
				OSHookBridge.ShowAdMobSmall(true);
				Debug.Log("admob ok");
				Destroy(gameObject);
			}


		}
		

		click = getString ("ad_network_click");
		Debug.Log ("img:"+imp);
		Debug.Log ("click:"+click);
		if (img == "") {
			Destroy (this);
		}
		else {
			StartCoroutine(ReciveImage(img));
		

				impression = true;
				OSHookBridge.SendRequst (imp);
				

		}
		
	}
public virtual	void Start() 
	{
		
		if (zoneNumber == zone.zone1)
			action = "zone1";
		if (zoneNumber == zone.zone2)
			action = "zone2";
		if (zoneNumber == zone.zone3)
			action = "zone3";
		if (zoneNumber == zone.onclose)
			action = "onclose";
		if (zoneNumber == zone.onstart)
			action = "onstart";
		if (zoneNumber == zone.session)
			action = "session";
		if (zoneNumber == zone.small)
			action = "small";
		Debug.Log ("action=" + action);
		
		if (gameObject.GetComponent<Renderer>()) {
			gameObject.GetComponent<Renderer>().enabled = false;
			Material newMat = Resources.Load ("Banner", typeof(Material)) as Material;
			gameObject.GetComponent<Renderer>().material = newMat;
		} else {
			Debug.LogWarning("There is no renderer on object.");
		}
		
	}
	
	
	public virtual IEnumerator ReciveImage(string img)
	{
		#if UNITY_EDITOR	
		Hashtable headers = new Hashtable();
		headers.Add("User-Agent", "Mozilla/5.0 (Linux; Android 4.4.2; Nexus 4 Build/KOT49H)");
		#endif
		
		//	Debug.Log ("recive image");
		if (img == null)
			Debug.Log ("url is null");
		
		WWW www = new WWW(img);
		www.threadPriority = ThreadPriority.High;
		yield return www;
		if (gameObject.GetComponent<Renderer>()) {
			gameObject.GetComponent<Renderer>().material.mainTexture = www.texture;
			loaded = true;
			gameObject.GetComponent<Renderer>().enabled = true;
		}
		if (gameObject.GetComponent<GUITexture>()) {
			gameObject.GetComponent<GUITexture>().texture = www.texture;
			loaded = true;
		}
	}
	
	public IEnumerator SetImage(Texture t)
	{
		if (gameObject.GetComponent<Renderer>()) {
			gameObject.GetComponent<Renderer>().material.mainTexture = t;
			loaded = true;
			gameObject.GetComponent<Renderer>().enabled = true;
		}
		if (gameObject.GetComponent<GUITexture>()) {
			gameObject.GetComponent<GUITexture>().texture = t;
			loaded = true;
		}
		return null;
	}
	
	
	void OnBecameVisible() {
		Debug.Log("impression fs banner");
		Impression ();
	}
	
	public virtual void  Update () {
		
		if (OSHookBridge.isStart())
			
		{
			
			if(!load)
			{
				Debug.Log("try get baner");
				
				StartCoroutine(loadBanner());
				
				load=true;
			}
			
			
		}
	}
	
	
	IEnumerator getBanner()
	{
		
		yield return null;
	}
	void OnRenderObject() {
		//
	}
	public void Impression()
	{
		
		if (!impression)
		{
			
			if(loaded == true)
				if (imp != "")
			{
				Debug.Log("impression fs banner");
				//OSHookBridge.SendRequst (imp);
				StartCoroutine(getRequst(imp));
				if (impAppsgeyser != "")
					StartCoroutine(getRequst(impAppsgeyser));
				
				impression=true;
			}
			
			
		}
	}
	void OnMouseOver()
	{
		
		if(Input.GetMouseButtonDown(0)){
			Debug.Log("onClick fs banner");
			
			if(click!="")
				Application.OpenURL(click);
			if(clickAppsgeyser!="")
				OSHookBridge.TrySendRequst(clickAppsgeyser);
		}
	}
	
	public IEnumerator getRequst(string url)
	{
		WWW www = new WWW(url);
		www.threadPriority = ThreadPriority.High;
		Debug.Log ("send req:"+url);
		yield return www;
	}

	protected string getString (string key)
	{
		
		if (reqJson != null) {
			IDictionary keys = (IDictionary)Json.Deserialize (reqJson);
			
			string text = (string)keys [key];
			return text;
			
		} else
			return null;
		
	}
	
	
	
}
