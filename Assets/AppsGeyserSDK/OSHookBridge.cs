﻿using System;
using System.Collections;
using System.Runtime.InteropServices;
using UnityEngine;

public class OSHookBridge {
	private static bool started=false;
	private static bool bannerIsOpen = false;
#if UNITY_ANDROID
	public static void ShowAdMobSmall(bool v){
		
		try {
			AndroidJavaClass ajc = new AndroidJavaClass ("com.appsgeyser.sdk.AppsgeyserSDK");
			ajc.CallStatic ("HideAdMobSmall", v);
		} catch (Exception e) {
			Debug.LogError ("cant hide admob");
			
		}
		
		
	}
	public static void showAdMobFullScreenBanner(string id,string click,string imp){

		try {
			AndroidJavaClass ajc = new AndroidJavaClass ("com.appsgeyser.sdk.AppsgeyserSDK");
			ajc.CallStatic ("showAdMobFullScreenBanner", id,click,imp);
		} catch (Exception e) {
			Debug.LogError ("cant open browser intent");

		}

		
	}
	public static void showAdMobFullScreenBannerEscape(string id,string click,string imp){
		
		try {
			AndroidJavaClass ajc = new AndroidJavaClass ("com.appsgeyser.sdk.AppsgeyserSDK");
			ajc.CallStatic ("showAdMobFullScreenBanner", id,click,imp,true);
		} catch (Exception e) {
			Debug.LogError ("cant open browser intent");
			
		}
		
		
	}
	public static void showAdMobAdSmall(string id,string click,string imp){
		
		try {
			AndroidJavaClass ajc = new AndroidJavaClass ("com.appsgeyser.sdk.AppsgeyserSDK");
			ajc.CallStatic ("showAdMobAdSmall", id,click,imp);
		} catch (Exception e) {
			Debug.LogError ("cant open browser intent");
			
		}
		
		
	}




	public static void Starting()
	{
		
		started = true;
	}
	public static bool isStart()
	{
		#if UNITY_EDITOR
		return true;
		#endif
		return started;
	}

	public static void SetBannerIsOpen(bool open)
	{
		bannerIsOpen = open;
	}
	public static bool GetBannerIsOpen()
	{
		return bannerIsOpen;
	}   


	public static bool isOnline(){
		try{
			AndroidJavaClass ajc = new AndroidJavaClass ("com.appsgeyser.sdk.AppsgeyserSDK");
			return ajc.CallStatic<bool> ("isOnline");
		}catch(Exception e){
			Debug.LogError("cant check is online");
			return true;//e.ToString();
		}
		
	}
	
	
	public static string GetUrl(){
		try{
			AndroidJavaClass ajc = new AndroidJavaClass ("com.appsgeyser.sdk.AppsgeyserSDK");
			return ajc.CallStatic<string> ("getURL");
		}catch(Exception e){
			Debug.LogError("dont get url");
			return e.ToString();
		}
		
	}
	public static string GetUnityUrl(){
		try{
			AndroidJavaClass ajc = new AndroidJavaClass ("com.appsgeyser.sdk.AppsgeyserSDK");
			return ajc.CallStatic<string> ("getUnityUrl");
		}catch(Exception e){
			Debug.LogError("dont get url");
			return e.ToString();
		}
		
	}
	public static string GetPackadgeName(){
		try{
			AndroidJavaClass ajc = new AndroidJavaClass ("com.appsgeyser.sdk.AppsgeyserSDK");
			return ajc.CallStatic<string> ("getPackadgeName");
		}catch(Exception e){
			Debug.LogError("dont PackadgeName");
			return e.ToString();
		}
		
	}
	public static string GetProductName(){
		try{
			AndroidJavaClass ajc = new AndroidJavaClass ("com.appsgeyser.sdk.AppsgeyserSDK");
			return ajc.CallStatic<string> ("getProductName");
		}catch(Exception e){
			Debug.LogError("dont ProductName");
			return e.ToString();
		}
		
	}
	
	public static void OpenBrowser(string url){
		try{
			AndroidJavaClass ajc = new AndroidJavaClass ("com.appsgeyser.sdk.AppsgeyserSDK");
			ajc.CallStatic ("openBrowser", url);
		}catch(Exception e){
			Debug.LogError("cant open browser intent");
			
		}
		
	}
	
	public static void RateUs(){
		
		try{
			AndroidJavaClass ajc = new AndroidJavaClass ("com.appsgeyser.sdk.AppsgeyserSDK");
			ajc.CallStatic ("rateUs");
		}catch(Exception e){
			Debug.LogError("cant open google play");
			
		}
		
	}
	
	public static string SendRequst(string url){
		if (started)
		try {
			AndroidJavaClass ajc = new AndroidJavaClass ("com.appsgeyser.sdk.AppsgeyserSDK");
			return ajc.CallStatic<string> ("sendRequst", url);
		} catch (Exception e) {
			Debug.LogError ("cant open browser intent");
			return e.ToString ();
		}
		else
			return null;
		
	}
	
	public static int getBundleVersion(){
		try {
			AndroidJavaClass ajc = new AndroidJavaClass ("com.appsgeyser.sdk.AppsgeyserSDK");
			return ajc.CallStatic<int> ("getBundleVersion");
		} catch (Exception e) {
			Debug.LogError ("dont get url");
			return -1;
		}
	}
	
	
	public static void SendUnityMessage(String objectName, String methodName, String parametrText){
		AndroidJavaClass ajc = new AndroidJavaClass ("com.appsgeyser.player.Bridge");
		ajc.CallStatic ("SendUnityMessage", objectName, methodName, parametrText);
	}
	
	public static bool SendApiKey(String apiKey){
		try{
			AndroidJavaClass ajc = new AndroidJavaClass ("com.appsgeyser.sdk.AppsgeyserSDK");
			return ajc.CallStatic<bool> ("sendApiKey", apiKey);
		}catch(Exception e){
			Debug.LogError("cant send api key");
			return false;
		}
	}
	
	public static bool GetBanner(String id,String msg){
		try{
			AndroidJavaClass ajc = new AndroidJavaClass ("com.appsgeyser.sdk.AppsgeyserSDK");
			return ajc.CallStatic<bool> ("getAppsgeyserBanner", id,msg);
		}catch(Exception e){
			Debug.LogError("cant get banner");
			return false;
		}
	}
	
	private static void ShowFullScreenBanner(){
		try{
			AndroidJavaClass ajc = new AndroidJavaClass ("com.appsgeyser.sdk.AppsgeyserSDK");
			ajc.CallStatic ("makeBanner");
		}catch(Exception e){
			Debug.LogError("cant open browser intent");
			
		}
	}
	public static void EscapeAlert(){
		try{
			AndroidJavaClass ajc = new AndroidJavaClass ("com.appsgeyser.sdk.AppsgeyserSDK");
			ajc.CallStatic ("escapeAlert");
		}catch(Exception e){
			Debug.LogError("cant  make escape alert");
			
		}
	}

	public static void EscapeAlertEnternalBanner(){
		try{
			AndroidJavaClass ajc = new AndroidJavaClass ("com.appsgeyser.sdk.AppsgeyserSDK");
			ajc.CallStatic ("escapeAlertEnternalBanner");
		}catch(Exception e){
			Debug.LogError("cant  make escape alert");
			
		}
	}
	private static bool BannerIsOpen(){
		try{
			AndroidJavaClass ajc = new AndroidJavaClass ("com.appsgeyser.sdk.AppsgeyserSDK");
			return ajc.CallStatic<bool> ("appsgeyserBannerIsOpen");
		}catch(Exception e){
			Debug.LogError("cant check banner");
			return false;
		}
	}
	
	private static void ShowFullScreenBanner(String id, String msg){
		try{
			AndroidJavaClass ajc = new AndroidJavaClass ("com.appsgeyser.sdk.AppsgeyserSDK");
			ajc.CallStatic ("makeBannerById",id, msg);
		}catch(Exception e){
			Debug.LogError("cant make fsbanner");
			
		}
	}
	private static void ShowFullScreenBanner(String id, String msg,String action){
		try{
			AndroidJavaClass ajc = new AndroidJavaClass ("com.appsgeyser.sdk.AppsgeyserSDK");
			ajc.CallStatic ("makeBannerByIdInAction",id, msg,action);
		}catch(Exception e){
			Debug.LogError("cant open browser intent");
			
		}
	}
	
	
	private static void CloseFullScreenBanner(){
		try{
			AndroidJavaClass ajc = new AndroidJavaClass ("com.appsgeyser.sdk.AppsgeyserSDK");
			ajc.CallStatic ("closeFullScreenBanner");
		}catch(Exception e){
			Debug.LogError("cant make banner");
			
		}
	}
	
	public static void TryCloseFullScreenBanner(){
		if(started)
		try{
			CloseFullScreenBanner();
		}catch{
			if(Application.platform == RuntimePlatform.Android)
				Debug.LogError("CloseFullScreenBanner() failed");
		}
	}
	public static void TryToShowBanner(){
		if(started)
		try{
			GameObject sdk=GameObject.Find("FullScreenObject");
			if (sdk == null) {
				sdk=GameObject.Instantiate(Resources.Load("FullScreenObject", typeof(GameObject))) as GameObject;
				sdk.name="FullScreenObject";
				FullScreenBannerSession fs=sdk.GetComponent<FullScreenBannerSession> ();
				
				fs.action="session";
			}

		}catch{
			if(Application.platform == RuntimePlatform.Android)
				Debug.LogError("ShowFullScreenBanner failed");
		}
	}


	public static void TryToShowBannerOnStart(){
#if UNITY_EDITOR
		started=true;
		#endif
		if(started)
		try{
			GameObject sdk=GameObject.Find("FullScreenObject");
			if (sdk == null) {
				sdk=GameObject.Instantiate(Resources.Load("FullScreenObjectOnStart", typeof(GameObject))) as GameObject;
				sdk.name="FullScreenObject";
			
			}
			
		}catch{
			if(Application.platform == RuntimePlatform.Android)
				Debug.LogError("ShowFullScreenBanner failed");
		}
	}
	public static void TryToShowBannerOnClose(){
		if(started)
		try{
			GameObject sdk=GameObject.Find("FullScreenObject");
			if (sdk == null) {
				sdk=GameObject.Instantiate(Resources.Load("FullScreenObjectOnClose", typeof(GameObject))) as GameObject;
				sdk.name="FullScreenObject";
				FullScreenBannerSession fs=sdk.GetComponent<FullScreenBannerSession> ();
				
				fs.action="onclose";
			}
			
		}catch{
			if(Application.platform == RuntimePlatform.Android)
				Debug.LogError("ShowFullScreenBanner failed");
		}
	}
	
	public static void TryToShowBanner(String id ,String msg){
		if(started)
		try{
			ShowFullScreenBanner(id,msg);
		}catch{
			if(Application.platform == RuntimePlatform.Android)
				Debug.LogError("ShowFullScreenBanner failed");
		}
	}
	public static void TryToShowBanner(String id ,String msg, String action){
		if(started)
		try{
			ShowFullScreenBanner(id,msg,action);
		}catch{
			if(Application.platform == RuntimePlatform.Android)
				Debug.LogError("ShowFullScreenBanner failed");
		}
	}
	public static void TryEscapeAlert(){
		if(started)
		try{
			EscapeAlert ();
		}catch{
			if(Application.platform == RuntimePlatform.Android)
				Debug.LogError("fail escape alert");
		}
	}
	
	public static bool TryCheckBannerIsOpen(){
		if (started)
		try {
			return BannerIsOpen ();
		} catch {
			if (Application.platform == RuntimePlatform.Android)
				Debug.LogError ("fail check is open banner");
			return false;
		}
		else
			return false;
	}
	
	public static string TrySendRequst(string url){
		if (started)
		try {
			return SendRequst (url);
		} catch {
			if (Application.platform == RuntimePlatform.Android)
				Debug.LogError ("fail send requst");
			return null;
		}
		else
			return null;
	}
	
	public static void TryOpenBrowser(string url){
		if(started)
		try{
			OpenBrowser(url);
		}catch{
			if(Application.platform == RuntimePlatform.Android)
				Debug.LogError("fail open browser");
			
		}
	}



	public static void TryShowSessionBanner()
	{
		TryToShowBanner ();
	}
	public static void TryShowSessionBanner(String msg)
	{
		TryToShowBanner();//TryToShowBanner (null, msg, "session");
	}
	public static void TryShowMenuBanner()
	{
		TryToShowBanner();//TryToShowBanner (null, "menu", "menu");
	}
	public static void TryShowMenuBanner(String msg)
	{
		TryToShowBanner();//TryToShowBanner(null ,msg, "menu");
	}
	
	
	public static void Quit()
	{
		if(GameObject.Find("FullScreenObject")==null)
		{
			EscapeAlertEnternalBanner();
			
		}
		else
		{
			if(((GameObject.Find("FullScreenObject").GetComponent<FullScreenBannerSession>())as FullScreenBanner).action=="onclose")
				GlobalObject.Instance.QuitApplication();

		}
	}
	
	
	
	
	#endif
}
