﻿using UnityEngine;
using System.Collections;

public class GameParameters
{
    //private static GameParameters _inst;
    //public static GameParameters inst
    //{
    //    get
    //    {
    //        if (_inst == null) _inst = new GameParameters();
    //        return _inst;
    //    }
    //}

    /// <summary>В игре используются уровни</summary>
    public static bool useLevels;

    /// <summary>В игре используются система звёзд на уровнях</summary>
    public static bool useStarsSystem;

    /// <summary>В игре используются очки</summary>
    public static bool useScores;


    /// <summary>Текущий уровень</summary>
    public static int currentLevel;

    /// <summary>Текущее значение очков</summary>
    public static int scoreCurrent;

    /// <summary>Рекорд очков</summary>
    public static int scoreHigh;
}
