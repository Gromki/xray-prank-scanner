﻿using UnityEngine;
using System.Collections;

public class GlobalMessage : MonoBehaviour {

	public static GlobalMessage Instance { get; private set; }
	private int LastNotificationId = 0;
	private int hour = 60 * 60; 
	void Start()
	{

	}
	void Awake() 
	{
		Instance = this;

	}

	public void PushAfter(int time, string title, string message)
	{
		if (Application.platform == RuntimePlatform.Android)
		{
			LastNotificationId = AndroidNotificationManager.instance.ScheduleLocalNotification(title, message, time);
		}
	}

	public void CancelNotifications()
	{	
		if (Application.platform == RuntimePlatform.Android)
		{
			AndroidNotificationManager.instance.CancelAllLocalNotifications();
			//AndroidToast.ShowToastNotification ("Notifications deleted!", AndroidToast.LENGTH_LONG);
		}

		GlobalObject.Instance.messageSend = false;
	}
}
