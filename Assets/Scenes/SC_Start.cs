﻿using UnityEngine;
using System.Collections;

public class SC_Start : MScene
{
    [SerializeField]
    bool isLevelsAvaliable;
    [SerializeField]
    bool isScoresAvaliable;

    protected override void Awake()
    {
        base.Awake();
        GameParameters.useLevels = isLevelsAvaliable;
        GameParameters.useScores = isScoresAvaliable;
    }

    public void BtnPlay_Click()
    {
        // при запуске загрузить нужную сцену
        //Application.LoadLevel(isLevelsAvaliable ? "Levels" : "Game");
		if (isLevelsAvaliable)
			Application.LoadLevel("Levels");
		else
			SC_Loading.StartLoadingScene("Game");
    }

    public override void OnBack_Click()
    {
        #if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
        #else
        //Application.Quit();
        #endif
    }
}
