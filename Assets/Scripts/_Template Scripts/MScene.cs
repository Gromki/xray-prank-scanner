﻿using UnityEngine;
using System.Collections;

public class MScene : MonoBehaviour
{

    protected virtual void Awake()
    { }


    protected void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            OnBack_Click();
    }

    protected void FixedUpdate()
    {

    }

    public virtual void OnBack_Click()
    { }


    public virtual void ButtonScaleDown(Transform sender)
    {
        UnityEngine.UI.Selectable slc = sender.GetComponent<UnityEngine.UI.Selectable>();
        if (slc!=null && slc.interactable)
        sender.localScale = new Vector3(0.85f, 0.85f, 1f);
    }

    public virtual void ButtonScaleUp(Transform sender)
    {
        UnityEngine.UI.Selectable slc = sender.GetComponent<UnityEngine.UI.Selectable>();
        if (slc != null && slc.interactable) 
            sender.localScale = new Vector3(1f, 1f, 1f);
    }
}
