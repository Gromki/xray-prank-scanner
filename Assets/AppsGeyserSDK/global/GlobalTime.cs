﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GlobalTime : MonoBehaviour 
{
	public static GlobalTime Instance { get; private set; }
	
	private string timeUrl = "http://www.timeapi.org/utc/now?format=%25Y-%25m-%25d%20%25H:%25M:%25S";
	
	int year;
	int month;
	int day;
	int hour;
	int minute;
	int seconds;
	
	private string timeString;
	private System.DateTime timeClass;
	
	public bool timeSeted = false;
	
	public void Awake()
	{
		Instance = this;
		StartCoroutine(checkTimeOverSeconds(5f));
	}
	
	IEnumerator checkTimeOverSeconds(float seconds)
	{
		while(true)
		{
			//Debug.Log("prepare time");
			prepareTime();
			yield return new WaitForSeconds(seconds);
		}
	}
	
	void prepareTime()
	{
		timeSeted = false;
		StartCoroutine(checkGlobalTime());
	}
	
	void setTime()
	{
		//Debug.Log("time: "+year+"-"+month+"-"+day+" "+hour+":"+minute+":"+seconds);
		
		timeString = year+"-"+month+"-"+day+" "+hour+":"+minute+":"+seconds;
		timeClass = new System.DateTime(year, month, day, hour, minute, seconds);
		timeSeted = true;
	}
	
	public string getTimeString() 
	{ 
		if(timeString == "" || timeString == null)
			timeString = System.DateTime.UtcNow.Year+"-"+System.DateTime.UtcNow.Month+"-"+System.DateTime.UtcNow.Day+" "+System.DateTime.UtcNow.Hour+":"+System.DateTime.UtcNow.Minute+":"+System.DateTime.UtcNow.Second;
		
		return timeString; 
	}
	
	public System.DateTime getTimeClass() 
	{
		System.DateTime tempTimeClass;
		
		if(timeClass != System.DateTime.MinValue)
			tempTimeClass = timeClass;
		else
			tempTimeClass = getTimeClassFast();
		
		return tempTimeClass; 
	}
	
	public System.DateTime getTimeClassFast() 
	{ 
		int localyear = System.DateTime.UtcNow.Year;
		int localmonth = System.DateTime.UtcNow.Month;
		int localday = System.DateTime.UtcNow.Day;
		
		int localhour = System.DateTime.UtcNow.Hour;
		int localminute = System.DateTime.UtcNow.Minute;
		int localseconds = System.DateTime.UtcNow.Second;
		
		System.DateTime localtimeClass = new System.DateTime(localyear, localmonth, localday, localhour, localminute, localseconds);
		
		return localtimeClass; 
	}
	
	void checkSystemTime()
	{
		year = System.DateTime.UtcNow.Year;
		month = System.DateTime.UtcNow.Month;
		day = System.DateTime.UtcNow.Day;
		
		hour = System.DateTime.UtcNow.Hour;
		minute = System.DateTime.UtcNow.Minute;
		seconds = System.DateTime.UtcNow.Second;
		
		//Debug.Log("SystemTime setTime");
		setTime ();
	}
	
	IEnumerator checkGlobalTime()
	{
		WWW www = new WWW(timeUrl);
		yield return www;
		
		if (www.error == null)
		{
			if(ParseTimeString(www.data))
			{
				//Debug.Log("GlobalTime setTime");
				setTime();
			}
			else
			{
				checkSystemTime();
			}
		} 
		else 
		{
			checkSystemTime();
		}    
	}

	bool ParseTimeString(string timeS)
	{
		//year+"-"+month+"-"+day+" "+hour+":"+minute+":"+seconds
		
		bool timeStringParsed = true;
		
		string [] split = timeS.Split(':',' ','-');
		if(split.Length > 5)
		{
			
			if(!int.TryParse(split[0], out year))
				timeStringParsed = false;
			if(!int.TryParse(split[1], out month))
				timeStringParsed = false;
			if(!int.TryParse(split[2], out day))
				timeStringParsed = false;
			
			if(!int.TryParse(split[3], out hour))
				timeStringParsed = false;
			if(!int.TryParse(split[4], out minute))
				timeStringParsed = false;
			if(!int.TryParse(split[5], out seconds))
				timeStringParsed = false;
			
		}
		else 
		{
			timeStringParsed = false;
		}  
		
		return timeStringParsed;
	}
	
}
