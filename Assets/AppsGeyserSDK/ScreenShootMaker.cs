﻿using UnityEngine;
using System.Collections;
using System.IO;
using System;

public class ScreenShootMaker : MonoBehaviour {
#if UNITY_EDITOR_WIN
    private string screenhotsFolderPath;
    public bool takeScreenShoots = false;

    void Awake() {
        DontDestroyOnLoad(gameObject);
    }

	void Start () {
        screenhotsFolderPath = Application.dataPath.Remove(Application.dataPath.Length - 7) + "/SreenShoots";
        Debug.Log(screenhotsFolderPath);
    }

    void Update() {
        if (takeScreenShoots)
        {
            if (!Directory.Exists(screenhotsFolderPath)) 
            {
                Directory.CreateDirectory(screenhotsFolderPath);
            }
            Application.CaptureScreenshot("SreenShoots/" + (DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds.ToString() + Time.frameCount.ToString() + ".png");
        }
    }
#endif
}
