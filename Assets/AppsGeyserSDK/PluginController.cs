﻿using System;
using System.Collections;
using System.Runtime.InteropServices;
using UnityEngine;
using System.Text;
using System.IO;  



public class PluginController : MonoBehaviour {

	public string apiKey;




	public void BannerOnClose(string msg)
	{
		OSHookBridge.TryToShowBannerOnClose ();
	}
	public void sdkStarted(string msg)
	{
		OSHookBridge.Starting();
		OSHookBridge.TryToShowBannerOnStart ();
		GameObject.Instantiate(Resources.Load("SmallBannerObject", typeof(GameObject)));

	}
	public  void closeBanner(string msg)
	{
		BroadcastAll("CloseBannerAppsgeyser",msg);
		if (msg == "escape" || msg == "adMobEscape") {
			GlobalObject.Instance.QuitApplication();				
				}
	}
	public  void openBanner(string msg)
	{
	
		BroadcastAll("OpenBannerAppsgeyser",msg);
	}
	public  void failBanner(string msg)
	{
		BroadcastAll("FailBannerAppsgeyser",msg);
		if (msg == "escape" || msg == "adMobEscape")
		{
			GlobalObject.Instance.QuitApplication();	
		}
	}
	void Awake(){
		Init();	
#if UNITY_EDITOR
		OSHookBridge.TryToShowBannerOnStart ();
#endif
	}

	void Init(){

	
		                                   


		if(GameObject.Find("__AppsgeyserSDK") != null){
			Destroy(gameObject);
		}else{
			DontDestroyOnLoad(gameObject);
			gameObject.name = "__AppsgeyserSDK";
		}


	}
	void Start () {
		if (apiKey == "" || apiKey == null) {
			Debug.LogError("Appsgeyser SDK: apiKey is empty or null. You should get apiKey on appsgeyser.com");		
		}

#if UNITY_ANDROID

		try{
			bool check = OSHookBridge.SendApiKey(apiKey);
			Debug.Log("Appsgeyser SDK is ON");
		}catch{
			if(Application.platform == RuntimePlatform.Android)
				Debug.LogError("Appsgeyser SDK failed");
		}
#endif
	//	Application.LoadLevel (1);
	}
	
	void Update () {
	
#if UNITY_ANDROID

	if(Input.GetKeyDown(KeyCode.Escape))
	{
		
			if(GameObject.Find("FullScreenObject")==null)
			{
				if(Application.loadedLevel == 0)
					OSHookBridge.EscapeAlertEnternalBanner();

			}
			else
			{
				if(((GameObject.Find("FullScreenObject").GetComponent<FullScreenBannerSession>())as FullScreenBanner).action=="onclose")
					GlobalObject.Instance.QuitApplication();
				Destroy(GameObject.Find("FullScreenObject"));
				OSHookBridge.TryCloseFullScreenBanner();
				OSHookBridge.ShowAdMobSmall(true);
			}
			/**
			if(OSHookBridge.TryCheckBannerIsOpen()) {
				OSHookBridge.TryCloseFullScreenBanner();
				OSHookBridge.SetBannerIsOpen(true);
				Invoke("CloseBanner",0.5f);
			}
		else
			if(Application.loadedLevel == 0)
				OSHookBridge.TryEscapeAlert();	
				**/
	}
			
#endif
	}

	void CloseBanner() {
		OSHookBridge.SetBannerIsOpen (false);
	}

	public void SetKey(string key){
		apiKey = key;
	}


	private const string TWITTER_ADDRESS = "http://twitter.com/intent/tweet";
	private const string TWEET_LANGUAGE = "en"; 
	
	void ShareToTwitter (string textToDisplay)
	{
		Application.OpenURL(TWITTER_ADDRESS +
		                    "?text=" + WWW.EscapeURL(textToDisplay) +
		                    "&amp;lang=" + WWW.EscapeURL(TWEET_LANGUAGE));
	}



	public static void BroadcastAll(string fun ,string msg) {
		GameObject[] gos = (GameObject[])GameObject.FindObjectsOfType(typeof(GameObject));
		foreach (GameObject go in gos) {
			if (go && go.transform.parent == null) {
				go.gameObject.BroadcastMessage(fun,msg, SendMessageOptions.DontRequireReceiver);
			}
		}
	}
}
