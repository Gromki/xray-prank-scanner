﻿using UnityEngine;
using System.Collections;

public class textureBannerController : MonoBehaviour {
	public GameObject[] BannerObject;
	int i = 0;
	// Use this for initialization
	void Start () {
		BannerObject[i].SetActive(true);

	}
	
	// Update is called once per frame
	void Update () {
	
		GameObject go=BannerObject[i];
		FullScreenBanner fs=(FullScreenBanner)go.GetComponent<FullScreenBanner>() as FullScreenBanner;
		if (fs.IsLouded ()) 
					{
			i++;
			BannerObject[i].SetActive(true);
					}
	}
}
